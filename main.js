"use strict";
// DOM - модель представления документа как совокупность обьектов, так же используется как посредник между языками программирования и коренным языком браузера
function alertArray(arr, whereToInsert = document.body) {
  let arr2 = arr.map((item) => `<li>${item}</li>`);

  let htmlCode = "";
  for (let item in arr2) {
    htmlCode += arr2[item];
  }

  let node = document.createElement("ul");
  node.innerHTML = htmlCode;

  whereToInsert.prepend(node);

  setTimeout(() => {
    node.remove();
  }, 3000);
}

let arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
let box = document.querySelector(".box");

alertArray(arr, box);
